package com.example.omnipark;

import java.sql.Time;

public class Slot {
    private boolean isEmpty;
    private String id;
    private String timeStart;
    private Driver driver;

    public Slot(String id, Driver driver, String timeStart){
        this.id = id;
        this.isEmpty = false;
        this.driver = driver;
        this.timeStart = timeStart;
    }

    public Slot (String id) {
        this.id = id;
        this.isEmpty = true;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = true;
        this.timeStart = null;
        this.driver = null;
    }

    public void setParking(Driver driver, String timeStart) {
        isEmpty = false;
        this.timeStart = timeStart;
        this.driver = driver;
    }

    public Driver getDriver() {
        return driver;
    }

    @Override
    public String toString() {
        if (isEmpty){
            return "ID: " + id  + "\nStatus: Empty";
        } else {
            return "ID: " + id
                    + "\nTime start: " + timeStart
                    + "\nDriver's name: " + driver.getName()
                    + "\nDriver's license plate: " + driver.getLicensePlates();
        }
    }
}
