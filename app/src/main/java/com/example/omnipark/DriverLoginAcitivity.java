package com.example.omnipark;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class DriverLoginAcitivity extends AppCompatActivity {

    private static final String TAG = "ContributorLoginAcitivity";

    private static Button btnTitle;
    private static Button btnLogin;
    private static TextView tvSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

    }
    private void init(){
        btnTitle = (Button) findViewById(R.id.btnTitle);
        btnTitle.setText("DRIVER");

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverLoginAcitivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        tvSignup = (TextView) findViewById(R.id.tvSignup);
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverLoginAcitivity.this, DriverSignupAcitivity.class);
                startActivity(intent);
            }
        });
    }

}






















