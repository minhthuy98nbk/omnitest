package com.example.omnipark;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class DriverSignupAcitivity extends AppCompatActivity {

    private static final String TAG = "DriverSignupAcitivity";

    private static Button btnTitle;
    private static Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_signup);
        init();

    }
    private void init(){

        btnSignup = (Button) findViewById(R.id.btnLogin);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverSignupAcitivity.this, DriverSignupCodeComfirmAcitivity.class);
                startActivity(intent);
            }
        });
    }

}






















