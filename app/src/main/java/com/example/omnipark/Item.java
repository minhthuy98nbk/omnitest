package com.example.omnipark;

public class Item {

    private String iconName;
    private String label;

    public Item(String iconName, String label){
        this.iconName = iconName;
        this.label = label;
    }

    public String getIconName() {
        return iconName;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "(" + iconName + "," + label + ")";
    }
}
