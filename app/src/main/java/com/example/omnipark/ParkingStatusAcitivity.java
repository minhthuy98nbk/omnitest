package com.example.omnipark;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;


public class ParkingStatusAcitivity extends AppCompatActivity {

    private static final String TAG = "ParkingStatusAcitivity";

    private static Button btnTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_status);
        init();

    }
    private void init(){
        List<Slot> image_details = getListData();
        final GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new CustomGridParkingStatusAdapter(this, image_details));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Object o = gridView.getItemAtPosition(position);
                Slot slot = (Slot) o;
                Toast.makeText(ParkingStatusAcitivity.this, "SELECTED::"
                        + " " + slot, Toast.LENGTH_LONG).show();
            }
        });
    }

    private  List<Slot> getListData() {
        List<Slot> list = new ArrayList<>();
        Driver driver = new Driver();
        driver.setLicensePlates("43K6-7864");
        list.add(new Slot("A001"));
        list.add(new Slot("A002"));
        list.add(new Slot("A003"));
        list.add(new Slot("A004", driver,"09h30"));
        list.add(new Slot("A006"));
        list.add(new Slot("A007"));
        return list;
    }

}






















