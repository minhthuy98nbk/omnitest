package com.example.omnipark;

import org.w3c.dom.DOMStringList;

public class Driver {
    private String name;
    private String phoneNumber;
    private String email;
    private String licensePlates;
    private String carBrand;
    private String carColor;
    private String address;
    private String password;

    public Driver(String name, String phoneNumber, String email, String licensePlates, String carBrand, String carColor, String address, String password){
        this.name =  name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.licensePlates = licensePlates;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.address = address;
        this.password = password;
    }

    public Driver(){}

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLicensePlates(String licensePlates) {
        this.licensePlates = licensePlates;
    }

    public String getLicensePlates() {
        return licensePlates;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }


    @Override
    public String toString() {
        return licensePlates;
    }
}
